import argparse
import pandas
import itertools
import sys
import os
import re
import numpy as np
from scipy.stats import fisher_exact

parser = argparse.ArgumentParser(description="Extract significantly co-occuring (co-excluding) species using Fisher's test")
parser.add_argument('input', help='Path to occurance table')
parser.add_argument('output', nargs="?", help='Tab separated output file')
parser.add_argument('--sep', type=str, default="\t", help='Column separator')
parser.add_argument('--threshold', type=str, default="0.0001", help='Threshold for presence detection (default: 0.0001). Multiple thresholds can be provided using ","')
parser.add_argument('--significance', type=float, default=0.05, help='Threshold for detecting significant hits')
parser.add_argument('--rownames', help='File with row names. Use None to ignore row names')
parser.add_argument('--colnames', help='File with column names. Use None to ignore col names')
parser.add_argument('--transposed', action="store_true", help='If provided the <species>x<samples> matrix is expected')
parser.add_argument('--part', dest='part', type=int, help='Which part is calculated (start from 1)', default=1)
parser.add_argument('--parts-total', dest='parts_total', type=int, help='Total number of parts', default=1)
parser.add_argument('--adjust', dest="adjust", default="",
    help="Multiple testing correction method (default: none). Allowed values: 'bonferroni', 'sidak', 'holm-sidak', "
         "'holm', 'simes-hochberg', 'hommel', 'fdr_bh', 'fdr_by', 'fdr_tsbh', 'fdr_tsbky'. "
         "See [statsmodels.sandbox.stats.multicomp.multipletests]")
parser.add_argument('--alternative', dest="alternative", choices={'two-sided', 'less', 'greater'}, default="two-sided",
    help='Which alternative hypothesis to the null hypothesis the test uses. Default is "two-sided"')
args = parser.parse_args()

if os.path.dirname(args.output) and not os.path.exists(os.path.dirname(args.output)):
    os.makedirs(os.path.dirname(args.output))

has_colnames = None if args.colnames or args.colnames=="None" else True
has_rownames = None if args.rownames or args.rownames=="None" else True
df = pandas.read_table(args.input, sep=args.sep, header=has_colnames, index_col=has_rownames)
print "Read occurrence table ({} x {})".format(df.shape[0], df.shape[1])

results = []

if args.colnames and args.colnames != "None":
    df.columns = [l.strip() for l in open(args.colnames, "r") if l.strip()]

if args.rownames and args.rownames != "None":
    df.index = [l.strip() for l in open(args.rownames, "r") if l.strip()]

if args.transposed:
    df = df.transpose()

tokens = [c for c in df.columns.values.tolist() if c != "sample"]
thresholds = []
for th in re.split(",", args.threshold):
    if "-" not in th and ":" not in th:
        thresholds.append(float(th))
    elif "-" in th and ":" in th:
        th = re.split("[-:]", th)
        for th1 in np.arange(float(th[0]), float(th[1]), float(th[2])):
            thresholds.append(th1)
    else:
        raise RuntimeError("Threshold value is incorrect")

actual_i = 0

with open(args.output.format(i=args.part, total=args.parts_total), "w") as file:
    for th_i, th in enumerate(thresholds):
        print "Threshold: {}".format(th)

        for tokens_i, tokens_pair in enumerate(itertools.combinations(tokens, 2), start=1):
            if tokens_i % args.parts_total != args.part - 1:
                continue

            if actual_i % 1000 == 0:
                print actual_i
                sys.stdout.flush()

            actual_i += 1

            token1, token2 = tokens_pair
            token1_bin = df[token1]>=th
            token2_bin = df[token2]>=th

            table_AB = (token1_bin & token2_bin).sum()
            table_A_ = (token1_bin & ~token2_bin).sum()
            table__B = (~token1_bin & token2_bin).sum()
            table___ = (~token1_bin & ~token2_bin).sum()

            table___x = table___ if table___>0 else 0.001
            table_A_x = table_A_ if table_A_>0 else 0.001
            table__Bx = table__B if table__B>0 else 0.001
            table_ABx = table_AB if table_AB>0 else 0.001
            odds2 = (table_ABx / float(table_A_x)) / float(table__Bx / float(table___x))
            odds, pvalue = fisher_exact([[table_AB, table_A_], [table__B, table___]], alternative=args.alternative)

            # print "[{}/{}]\n{}|{}\t{},{},{},{} : {}".format(
            #     th_i * len(tokens_combinations) + tokens_i,
            #     len(thresholds) * len(tokens_combinations),
            #     token1, token2, table_AB, table_A_, table__B, table___, pvalue)
            from collections import OrderedDict
            row = OrderedDict([
                ("threshold", th),
                ("tokens", "{}|{}".format(token1, token2)),
                ("table_AB", table_AB), ("table_A_", table_A_), ("table__B", table__B), ("table___", table___),
                ("pvalue", pvalue), ("odds", odds2)])
            results.append(row)
            file.write("\t".join(str(v) for v in row.itervalues()) + "\n")

cols_order = ['threshold', 'tokens', 'table_AB', 'table_A_', 'table__B', 'table___', "pvalue", "odds"]
if args.adjust:
    from statsmodels.sandbox.stats.multicomp import multipletests
    cols_order.append("pvalue_adj")
    pvalues = [r['pvalue'] for r in results]
    reject, pvals_adj, alpha_sidak, alphac_bonf = multipletests(pvalues, alpha=args.significance, method=args.adjust)
    for i, pvalue_adj in enumerate(pvals_adj):
        results[i]["pvalue_adj"] = pvalue_adj

results_df = pandas.DataFrame(results)
results_df = results_df[cols_order]

# Print and write output
pandas.set_option("display.width", 120)
print results_df.query("pvalue{} < {}".format("_adj" if args.adjust else "", args.significance))

